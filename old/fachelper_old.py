#!/usr/bin/env python3

#data
buildings = {
	#'':{'mk':, 'in':, 'out':, 'time':, 'crafting_speed':},
	'Ore Crusher Saphirite to Crushed Saphirite':{'mk':1, 'in':8, 'out':8, 'time':3, 'crafting_speed':1.5},
	'Ore Crusher Saphirite to Crushed Stone':{'mk':1, 'in':8, 'out':2, 'time':3, 'crafting_speed':1.5},
	'Ore Sorting Facility Saphirite to Iron Ore':{'mk':1, 'in':8, 'out':4, 'time':1, 'crafting_speed':0.75},
	'Ore Sorting Facility Saphirite to Copper Ore':{'mk':1, 'in':8, 'out':2, 'time':1, 'crafting_speed':0.75},
	#'Assembling Machine Crushed Stone to Stone':{'mk':1, 'in':3, 'out':1, 'time':2, 'crafting_speed':0.5},
	'Assembling Machine Crushed Stone to Stone':{'mk':2, 'in':3, 'out':1, 'time':2, 'crafting_speed':0.75},

	'Ore Processor Iron Ore to Processed':{'mk':1, 'in':6, 'out':1, 'time':4.33, 'crafting_speed':0.75},
	'Blast Furnace Processed Iron to Ingot':{'mk':1, 'in':1, 'out':24, 'time':12, 'crafting_speed':0.75},
	
	'Blast Furnace Iron Ore to Ingot':{'mk':1, 'in':6, 'out':18, 'time':8.5, 'crafting_speed':0.75},
	'Induction Furnace Iron Ingot to Molten':{'mk':1, 'in':5, 'out':5, 'time':8.5, 'crafting_speed':0.75},
	'Casting Machine Molten Iron to Plate':{'mk':1, 'in':4, 'out':4, 'time':8.0, 'crafting_speed':0.75},
	
	'Blast Furnace Iron to Steel Ingot':{'mk':1, 'in':9, 'out':5, 'time':8.5, 'crafting_speed':0.75},
	'Induction Furnace Steel Ingot to Molten':{'mk':1, 'in':4, 'out':4, 'time':8.0, 'crafting_speed':0.75},
	'Casting Machine Molten Steel to Plate':{'mk':1, 'in':4, 'out':4, 'time':8.0, 'crafting_speed':0.75},
	
	'Ore Processor Copper Ore to Processed':{'mk':1, 'in':3, 'out':1, 'time':3.66, 'crafting_speed':0.75},
	'Blast Furnace Processed Copper to Ingot':{'mk':1, 'in':1, 'out':24, 'time':10, 'crafting_speed':0.75},
	
	'Blast Furnace Copper Ore to Ingot':{'mk':1, 'in':3, 'out':18, 'time':7.33, 'crafting_speed':0.75},
	'Induction Furnace Copper Ingot to Molten':{'mk':1, 'in':5, 'out':5, 'time':7.33, 'crafting_speed':0.75},
	'Casting Machine Molten Copper to Plate':{'mk':1, 'in':4, 'out':4, 'time':8, 'crafting_speed':0.75},
	
	'Electronics Assembling Machine Copper Plate to Wires':{'mk':1, 'in':10, 'out':2, 'time':2, 'crafting_speed':1.0},
	
	'Oil&Gas Separator, Natural Gas Sepp., NG to Raw Gas':{'mk':1, 'in':10, 'out':6, 'time':2, 'crafting_speed':1.0},
	'Gas Refinery, RG to NGL':{'mk':1, 'in':10, 'out':8, 'time':6, 'crafting_speed':1.0},
	'Gas Refinery, NGL to Methane':{'mk':1, 'in':10, 'out':5, 'time':6, 'crafting_speed':1.0},
	'Steam Cracker, Methane to Methanol':{'mk':1, 'in':6, 'out':8, 'time':4, 'crafting_speed':1.0},
	'Steam Cracker, Methanol to Propene':{'mk':1, 'in':10, 'out':8, 'time':2, 'crafting_speed':1.0},
	'Chem Plant Propene Gas to Plastic':{'mk':1, 'in':4, 'out':1, 'time':2, 'crafting_speed':1.25},
	
	'Liquifier Slag to Slag Slurry':{'mk':1, 'in':5, 'out':5, 'time':3, 'crafting_speed':1.5},
	'Filtration Unit Slag Slurry to Min Slurry':{'mk':1, 'in':5, 'out':5, 'time':4, 'crafting_speed':1.5},
	'Crystallize to Iron&Copper':{'mk':1, 'in':5, 'out':0.4, 'time':8, 'crafting_speed':1.5},
}

belts = {'blue':40.0, 'red':26.67, 'yellow':13.33}


class Fachelper:
	def calculate_full_process(self, target_belt, belt_fullness, process_list):
		target_amount = belts[target_belt]*belt_fullness
		
		print("to produce %.2f full %s belts, or %.2f items per second, you need:" % (belt_fullness, target_belt, target_amount))
		previous_step_out_required = target_amount
		for building_name in process_list:
			d = buildings[building_name]
			self.calc_per_sec(building_name)
			#print("%s in_per_sec: %2.3f" % (building_name, d['in_per_sec']))
			#print("%s out_per_sec: %2.3f" % (building_name, d['out_per_sec']))
		
		for building_name in reversed(process_list):
			d = buildings[building_name]
			
			d['required_building_qty'] = previous_step_out_required/d['out_per_sec']
			previous_step_out_required = d['required_building_qty']*d['in_per_sec']
			print("need %.2f %s" % (d['required_building_qty'], building_name))
		
		print("main input needed: %.2f" % previous_step_out_required)
		print('\n')
	
	def calc_per_sec(self, building_name):
		d = buildings[building_name]
		in_per_sec = d['in'] * d['crafting_speed'] / d['time']
		out_per_sec = d['out'] * d['crafting_speed'] / d['time']
		d['in_per_sec'] = in_per_sec
		d['out_per_sec'] = out_per_sec

fh = Fachelper()

fh.calculate_full_process("yellow", 0.8, ('Ore Crusher Saphirite to Crushed Saphirite', ))

fh.calculate_full_process("yellow", 0.2/3, ('Assembling Machine Crushed Stone to Stone', ))

#fh.calculate_full_process("yellow", 1.0, ('Blast Furnace Iron Ore to Ingot', 'Induction Furnace Iron Ingot to Molten', 'Casting Machine Molten Iron to Plate'))
fh.calculate_full_process("yellow", 1.0, ('Ore Processor Iron Ore to Processed', 'Blast Furnace Processed Iron to Ingot', 'Induction Furnace Iron Ingot to Molten', 'Casting Machine Molten Iron to Plate'))

fh.calculate_full_process("yellow", 2/3, ('Blast Furnace Iron Ore to Ingot', 'Blast Furnace Iron to Steel Ingot', 'Induction Furnace Steel Ingot to Molten', 'Casting Machine Molten Steel to Plate'))

fh.calculate_full_process("yellow", 1.2, ('Ore Processor Copper Ore to Processed', 'Blast Furnace Processed Copper to Ingot', 'Induction Furnace Copper Ingot to Molten', 'Casting Machine Molten Copper to Plate'))
#fh.calculate_full_process("yellow", 1.0, ('Blast Furnace Copper Ore to Ingot', 'Induction Furnace Copper Ingot to Molten', 'Casting Machine Molten Copper to Plate'))

print("only 1/5th of a belt so internally 1 yellow belt will be enough")
fh.calculate_full_process("yellow", 0.2, ('Blast Furnace Copper Ore to Ingot', 'Induction Furnace Copper Ingot to Molten', 'Casting Machine Molten Copper to Plate', 'Electronics Assembling Machine Copper Plate to Wires'))

fh.calculate_full_process("yellow", 0.15, ('Oil&Gas Separator, Natural Gas Sepp., NG to Raw Gas', 'Gas Refinery, RG to NGL', 'Gas Refinery, NGL to Methane', 'Steam Cracker, Methane to Methanol', 'Steam Cracker, Methanol to Propene', 'Chem Plant Propene Gas to Plastic'))

fh.calculate_full_process("yellow", 0.075, ('Liquifier Slag to Slag Slurry', 'Filtration Unit Slag Slurry to Min Slurry', 'Crystallize to Iron&Copper'))
