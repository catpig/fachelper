This may seem long, but I just tried to give detailed instructions. I probably failed, so let me know where ;)

# SETUP
1. In factorio save your game
2. Activate the factorio console (if not done already) by opening it and typing "/c" (without the quotes) and pressing enter, twice.
3. Go to https://raw.githubusercontent.com/jforr/factorio/patch-1/dump_recipes/dump_recipes.lua with your browser and copy the whole file into the clipboard (e.g. by pressing Ctrl+A and then Ctrl+C).
4. Open the factorio console again, paste (with Ctrl+V) and press enter. The game may appear to freeze for a few seconds - it hasn't, it's just processing all the recipes for you.
5. If you're trying to unlock achievements reload your game now. If not you can just keep playing
6. Now you can use fachelper. If your recipes change (for example due to adding, removing or updating mods) just rerun the above steps.

# USING FACHELPER
The tool can currently do one thing. Copy ELB.py to something (like "my_production.py") and open it in a text editor (can't use notepad.exe from Windows, try Notepad++ for example). You need to make the following edits:

1. Change out_name to the product you want to create. You have to use the internal name precisely, for example "copper-cable" instead of "Copper cable".
2. Change out_aim_per_second to the number of items you want to make per second. You can just write in a number here (for example a yellow belt can carry 13.33 items per second) or you can use fractions or any other python code that produces a number.
3. Change in_infinite to be a list of the things that are provided separately and do not need to be calculated by the tool. Note that if there's only one input provided you need to write ('input-name', ) like in the example, with the comma and brackets like that.
4. For each process step make a new line like in the example. The first string is the name of the recipe. Again, this has to be the precisely correct internal name. Then comes the name of the machine you're using exactly like in the file data.py. Finally comes the level (or "mk") of the building as a number.
5. That's it! Now just execute your python file from a command prompt (aka terminal/console/DOS window/shell) and it'll print out what you need to build in reverse order of production. If there's some problem it should tell you what went wrong.

# KNOWN PROBLEMS
1. There's no GUI.
2. Currently only supports the GOG version on Linux out of the box. If you run a different version please tell me the full path to your "script-output" folder and I'll update this to support your version, too.
3. It doesn't react well to flawed input. If you ever get a stack trace please send it to me and I'll try to add better handling.
4. The buildings' crafting speed has to be manually recorded in data.py.
5. There's some imperfections in the "translation" of display names to internal names

# QUESTIONS/SUPPORT
I'm catpig/catpigger/catpiggest in IRC on freenode in #factorio. Alternatively you can e-mail me. Note that I may take a long time to reply to mails.

