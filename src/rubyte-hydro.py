#!/usr/bin/env python3

from fachelper import Fachelper

fh = Fachelper()

out_aim = 'Gold ore'
out_aim_per_second = 20.0
in_infinite = ('Rubyte Ore', 'Purified Water')
never_output = ()#'Nitric Waste Water', 'Slag', 'Crushed Stone')
process = (
	('Rubyte Ore Crushing', 'Ore Crusher', 2),
	('Rubyte Hydro Refining', 'Floatation Cell', 1),
	('Rubyte Chunks Sorting', 'Ore Sorting Facility', 2),
	('Slag Processing to Stone', 'Ore Crusher', 2),
	('Nitric Waste Water Purification', 'Hydro Plant', 1),
)
result = fh.calc_whole_process(out_aim, out_aim_per_second, in_infinite, never_output, process)
fh.print_whole_process(result)


print("\n\n\n")
out_aim = 'Purified Water'
out_aim_per_second = 35.0
in_infinite = ('Nitric Waste Water', )
never_output = ()#'Nitric Waste Water', 'Slag', 'Crushed Stone')
process = (
	('Nitric Waste Water Purification', 'Hydro Plant', 1),
)
result = fh.calc_whole_process(out_aim, out_aim_per_second, in_infinite, never_output, process)
fh.print_whole_process(result)


print("\n\n\n")
print("leftover: 20 slag+20 crushed stone\n10 ore crusher mk2 turn that into 60 crushed stone total per s\n30 assembler mk3 turn that into 20 stone/s")

#out_aim = 'Crushed Stone'
#out_aim_per_second = 26+2/3
#in_infinite = ('Slag', )
#never_output = ()#'Nitric Waste Water', 'Slag', 'Crushed Stone')
#process = (
	#('Slag Processing to Stone', 'Ore Crusher', 2),
##	('Stone', 'Assembling Machine', 3),
#)
#result = fh.calc_whole_process(out_aim, out_aim_per_second, in_infinite, never_output, process)
#fh.print_whole_process(result)


#print("\n\n\n")
#print("finally the stone from the crushing")
#out_aim = 'Stone'
#out_aim_per_second = 13+1/3
#in_infinite = ('Crushed Stone', )
#never_output = ()#'Nitric Waste Water', 'Slag', 'Crushed Stone')
#process = (
	#('Stone', 'Assembling Machine', 3),
#)
#result = fh.calc_whole_process(out_aim, out_aim_per_second, in_infinite, never_output, process)
#fh.print_whole_process(result)


