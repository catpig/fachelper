#!/usr/bin/env python3

# Copyright (C) Steffen Schaumburg 2016-2017 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

from argparse import ArgumentParser
from configparser import ConfigParser, MissingSectionHeaderError
import json
import os
import sys

from data import buildings

FACTORIO_ROOT = os.path.join(os.environ['HOME'], 'GOG Games', 'Factorio Beta', 'game')
LOCALE = 'en'

class Fachelper:
	def __init__(self):
		argparser = ArgumentParser(description='fachelper', epilog='Run this file directly for making searches, use a separate file to make calculations (see ELB.py for an example)')
		argparser.add_argument('-l', '--localised', help='Search localised names for the given string (case-insensitive)')
		argparser.add_argument('-i', '--input', help='Search for the given item name as a recipe input')
		argparser.add_argument('-o', '--output', help='Search for the given item name as a recipe output')
		args = argparser.parse_args()
		#print(args)
	
		self.localised_names = {}
		self.localised_names_reverse = {}
		
		self.load_recipes(os.path.join(FACTORIO_ROOT, 'script-output', 'recipes'))
		self.load_localised_names(FACTORIO_ROOT)
		self.save_recipes('recipes.ini')
		
		if args.localised:
			for key, value in self.localised_names.items():
				if args.localised.lower() in key.lower():
					print(key, '=', value)
				if args.localised.lower() in value.lower():
					print(key, '=', value)
		if args.input:
			print("TODO input:", args.input)
		if args.output:
			output_internal = self.localised_names_reverse[args.output]
			print("output_internal:", output_internal)
			for recipe_name, recipe in self.recipes.items():
				for out in recipe['products'].keys():
					if output_internal in out:
						print("found %s as output of recipe %s" % (args.output, recipe_name))
	
	def calc_per_sec(self, recipe_name, building_name, building_level):
		#print("calc_per_sec: building_name", building_name, recipe_name)
		crafts_per_s = 1/(self.recipes[recipe_name]['energy']/buildings[building_name][building_level])
		#print("crafts_per_s",crafts_per_s,"recipetime", self.recipes[recipe_name]['time'], 'craftspeed', buildings[building_name][building_level])
		
		inserters = {}
		ins = {}
		for ingredient in self.recipes[recipe_name]['ingredients']:
			#print("in ingredient",ingredient)
			ins[ingredient['name']] = ingredient['amount']*crafts_per_s
			inserters[ingredient['name']] = ingredient['amount']*crafts_per_s
		
		outs = {}
		for product in self.recipes[recipe_name]['products']:
			#print("out prodct",product)
			#print("out name, amount",product['name'], product['amount'])
			outs[product['name']] = product['amount']*crafts_per_s
			inserters[product['name']] = product['amount']*crafts_per_s
		
		return {'ins':ins, 'outs':outs, 'inserters':inserters}
		
	
	def calc_whole_process(self, out_aim, out_aim_per_second, in_infinite_display_names, never_output_display_names, process_display_names):
		#convert to internal names
		out_aim = self.localised_names_reverse[out_aim]
		
		in_infinite = []
		for inf in in_infinite_display_names:
			in_infinite.append(self.localised_names_reverse[inf])
		
		never_output = []
		for inf in never_output_display_names:
			never_output.append(self.localised_names_reverse[inf])
		
		process = []
		for recipe_name, building_name, building_level in process_display_names:
			if recipe_name == "Glass": #TODO remove this horrid hack and fix localisation load instead
				process.append(("quartz-glass", building_name, building_level))
			else:
				process.append((self.localised_names_reverse[recipe_name], building_name, building_level))
		
		#setup output dicts
		ins_needed = {out_aim:out_aim_per_second}
		outs_produced = {}
		steps = ""
		
		while not self.is_output_complete(ins_needed, in_infinite, outs_produced, never_output):
			#print("ins_needed:",ins_needed)
			found_something = False
			for recipe_name, building_name, building_level in process:
				product_names = []
				if recipe_name not in self.recipes.keys():
					print("unknown recipe name:", recipe_name)
					exit(1)
				
				#print(self.recipes[recipe_name]['products'])
				for product in self.recipes[recipe_name]['products']:
					#print(product)
					for key, value in product.items():
						if key == 'name':
							product_names.append(value)
							continue
				#print("list of product_names:", product_names)
				
				#TODO change here to allow output bans
				for in_needed_name in ins_needed.keys():
					if (in_needed_name not in in_infinite) and (in_needed_name in product_names):
						step_results = self.calc_per_sec(recipe_name, building_name, building_level)
						step_machine_count = ins_needed[in_needed_name]/step_results['outs'][in_needed_name]
						
						for key,value in step_results['outs'].items():
							if key in ins_needed:
								ins_needed.pop(key)
							else:
								if key in outs_produced:
									outs_produced[key] += value*step_machine_count
								else:
									outs_produced[key] = value*step_machine_count
						
						for key,value in step_results['ins'].items():
							if key in ins_needed:
								ins_needed[key] += value*step_machine_count
							else:
								ins_needed[key] = value*step_machine_count
						
						inserter_string = ""
						for key, value in step_results['inserters'].items():
							inserter_string += "%.2f/s %s, " % (value, self.localised_names[key])
						inserter_string = inserter_string[:-2]
						
						try:
							step_string = "%.2f %s Mk%d doing %s. inserters: %s\n" % (step_machine_count, building_name, building_level, self.localised_names[recipe_name], inserter_string)
						except KeyError: #TODO remove this hack
							step_string = "%.2f %s Mk%d doing %s. inserters: %s\n" % (step_machine_count, building_name, building_level, recipe_name, inserter_string)
						#print("step_string",step_string)
						steps += step_string
						found_something = True
						break
			if not found_something:
				unsatisfied_needs = []
				for needed in ins_needed:
					if needed not in in_infinite:
						unsatisfied_needs.append(needed)
				if len(unsatisfied_needs) > 0:
					print("Missing requirements, please add process steps or in_infinite:")
					for needed in unsatisfied_needs:
						print("\t, '%s'" % self.localised_names[needed])
				
				produced_nevers = []
				for out_name, amount in outs_produced.items():
					if amount > 0.0 and out_name in never_output:
						produced_nevers.append(out_name)
				if len(produced_nevers) > 0:
					print("producing banned outputs, please add process steps or remove from never_output:")
					for out_name in produced_nevers:
						print("\t, '%s'" % self.localised_names[needed])
				
				
				exit(1)
		
		outs_produced[out_aim] = out_aim_per_second
		return (ins_needed, outs_produced, steps)
	
	def is_output_complete(self, ins_needed, in_infinte, outs_produced, never_output):
		for prod, amount in ins_needed.items():
			if amount > 0.0 and prod not in in_infinte:
				return False
		
		#for out_name, amount in outs_produced.items():
		#	#print("out_name, amount",out_name, amount )
		#	if amount > 0.0 and out_name in never_output:
		#		return False
		
		return True
	
	def load_localised_names(self, load_path):
		self.load_localisition_file(os.path.join(load_path, 'data', 'base', 'locale', LOCALE, 'base.cfg'))
		
		for mod_dir_name in os.listdir(os.path.join(load_path, 'mods')):
			#print("mod_dir_name:",mod_dir_name)
			if os.path.isdir(os.path.join(load_path, 'mods', mod_dir_name)):
				mod_loc_path = os.path.join(load_path, 'mods', mod_dir_name, 'locale', LOCALE)
				try:
					for filename in os.listdir(mod_loc_path):
						if filename.endswith('.cfg'):
							self.load_localisition_file(os.path.join(mod_loc_path, filename))
							#print(sys.getsizeof(self.localised_names))
				except FileNotFoundError:
					pass
	
	def load_localisition_file(self, load_path):
		parser = ConfigParser(interpolation=None)
		try:
			parser.read(load_path)
		except MissingSectionHeaderError:
			with open(load_path, 'r') as infile:
				for line in infile.readlines():
					try:
						key, value = line.split('=')
						self.localised_names[key] = value
						self.localised_names[value] = key
					except ValueError:
						pass
		
		self.add_parser_to_dict(parser)
	
	def load_recipes(self, load_path):
		with open(load_path) as in_file:
			self.recipes = json.load(in_file)
		for recipe_name, recipe in self.recipes.items():
			new_products = []
			for out in recipe['products']:
				if not 'probability' in out.keys():
					new_products.append({'amount':out['amount'], 'name':out['name']})
					continue
				
				if 'amount' in out.keys():
					new_products.append({'amount':out['amount']*out['probability'], 'name':out['name']})
					continue
				
				if out['probability'] == 0:
					new_products.append({'amount':out['amount_min']*out['probability'], 'name':out['name']})
					continue
				
				new_products.append({'amount':out['amount_min']*(1-out['probability'])+out['amount_max']*(out['probability']), 'name':out['name']}) #TODO check whether amount calculation is like how factorio does it
			recipe['products'] = new_products
	
	def add_parser_to_dict(self, parser):
		for section in parser.sections():
			#print(section, parser.items(section))
			for key,value in parser.items(section):
				self.localised_names[key] = value #note that i'm flattening one level of hierarchy - this may lead to problems, we'll just have to see
				self.localised_names_reverse[value] = key #TODO this is obviously dumb, but feeling lazy right now
	
	def print_whole_process(self, args):
		ins_needed = args[0]
		outs_produced = args[1]
		steps = args[2]
		
		inputs_str = "Inputs required: "
		for key, value in args[0].items():
			inputs_str += "%.2f %s, " % (value, self.localised_names[key])
		inputs_str = inputs_str[:-2]
		print(inputs_str + '\n')
		
		outputs_str = "Outputs produced: "
		for key, value in args[1].items():
			outputs_str += "%.2f %s, " % (value, self.localised_names[key])
		outputs_str = outputs_str[:-2]
		print(outputs_str + '\n')
		
		print("Steps taken:")
		print(args[2])
	
	def save_recipes(self, save_path):
		with open(save_path, 'w') as out_file:
			for recipe_name, recipe_details in self.recipes.items():
				out_string = "\n[%s]\n" % recipe_name
				for key, value in recipe_details.items():
					out_string += "%s = %s\n" % (key, value)
				
				out_file.write(out_string)

#TODO test suit
if __name__ == '__main__':
	fh = Fachelper()
#print("correct:    {'ins': {'Molten Steel': 0.375}, 'outs': {'Steel Plate': 0.375}}")
#print("calculated:", fh.calc_per_sec('Steel Plate making', 'Casting Machine', 1))
