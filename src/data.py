# Copyright (C) Steffen Schaumburg 2016-2017 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

belts = { #dict[str:float] meaning [colour:transport throughput]
	'blue':40.0,
	'red':80/3,
	'yellow':40/3,
}

buildings = { #dict[str:dict[int:float]] meaning [name:[mk:crafting speed]]
	'Assembling Machine': {1:0.5, 2:0.75, 3:1.25},
	'Blast Furnace': {1:0.75, 2:1.25},
	'Casting Machine': {1:0.75, 2:1.0},
	'Chemical plant': {1:1.25, },
	'Electric Furnace': {1:2.0},
	'Electric Metal-mixing Furnace': {1:2.0},
	'Electrolyser': {1:1.0},
	'Electronics Assembling Machine': {1:1.0, 2:2.25},
	'Floatation Cell': {1:0.75, },
	'Hydro Plant': {1:1.0, },
	'Induction Furnace': {1:0.75, 2:1.0},
	'Ore Crusher': {1:1.5, 2:2.0},
	'Ore Processing Machine': {1:0.75, 2:1.0},
	'Ore Sorting Facility': {1:0.75, 2:1.0},
	'Pellet Press': {1:0.75, 2:1.0},
	'Steam Cracker': {1:1.0, },
}
